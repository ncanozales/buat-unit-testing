import React from 'react';
import { Link } from 'react-router-dom';

export default function Belajar() {
  // Belajar React
  return (
    <main>
      <div>
        <h1 className="text-blue-500">Maulana Akbar Ramadhan</h1>
        <Link to="/">Kembali ke Main</Link>
      </div>
    </main>
  );
}
