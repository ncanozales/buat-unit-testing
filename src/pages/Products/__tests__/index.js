import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import Products from '../Products';
import { useLocation } from 'react-router-dom';

jest.mock('react-router-dom', () => {
  return {
    useLocation: jest.fn(() => {
      return jest.fn();
    }),
  };
});

describe('src/pages/Products', () => {
  test('render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Products />);
    expect(tree).toMatchSnapshot();
  });
});
