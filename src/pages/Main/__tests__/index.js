import React from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import ShallowRenderer from 'react-test-renderer/shallow';
import Main from '../Main';

jest.mock('react-redux', () => {
  return {
    useDispatch: jest.fn(() => {
      return jest.fn();
    }),

    useSelector: jest.fn((x) => {
      x({ main: 'main' });
      return {
        idx: 0,
        subtitles: ['one'],
      };
    }),
  };
});

jest.mock('../../../reducers/main', () => {
  return {
    next: jest.fn(),
  };
});

jest.mock('../styles.scoped.css', () => ({
  root: 'root',
}));

describe('src/pages/main', () => {
  test('render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Main />);
    expect(tree).toMatchSnapshot();
  });

  test('when idx > 0', () => {
    useSelector.mockImplementationOnce(() => ({
      idx: 1,
      subtitles: ['one', 'two'],
    }));
    const res = Main();
    const div = res.props.children;
    const p = div.props.children[1];
    expect(p.props.children).toBe('two');
  });

  test('onClick button', () => {
    const dispatch = jest.fn();
    useDispatch.mockImplementationOnce(() => dispatch);
    const res = Main();
    const div = res.props.children;
    const button = div.props.children[2];
    button.props.onClick();
    expect(dispatch).toHaveBeenCalled();
  });
});
