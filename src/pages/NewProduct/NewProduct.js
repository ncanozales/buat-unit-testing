import React, { useState } from 'react';
import Form from '../../components/elements/Form/Form';
import './NewProduct.css';
import InputFIeld from '../../components/elements/InputField/InputFIeld';
import Select from '../../components/elements/Select/Select';
import TextArea from '../../components/elements/TextArea/TextArea';
import { useLocation, useNavigate } from 'react-router-dom';

export default function NewProduct() {
  const [productName, setProductName] = useState('');
  const [productPrice, setProductPrice] = useState(0);
  const [productCategory, setProductCategory] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [productHasExpiry, setProductHasExpiry] = useState(false);
  const [productExpiryDate, setProductExpiryDate] = useState(null);
  const [productImage, setProductImage] = useState('');

  const location = useLocation();
  const currentLocation = location.pathname;

  const navigate = useNavigate();

  const onChangeName = (e) => {
    setProductName(e.target.value);
  };

  const onChangePrice = (e) => {
    setProductPrice(e.target.value);
  };

  const onChangeCategory = (e) => {
    setProductCategory(e.target.value);
  };

  const onChangeDescription = (e) => {
    setProductDescription(e.target.value);
  };

  const onChangeHasExpiry = () => {
    setProductHasExpiry(!productHasExpiry);
  };

  const onChangeExpiryDate = (e) => {
    setProductExpiryDate(e.target.value);
  };

  const onChangeImage = async (event) => {
    const file = event.target.files[0];
    const base64 = await convertBase64(file);
    setProductImage(base64);
  };

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  // TASK YANG INI YA BAR MAKASIH
  const onSubmitHandler = ()=>{
    const newItem = {
      'id': 'sku2813',
      'name': productName,
      'price': productPrice,
      'image': productImage,
      'description': productDescription,
      'isDeleted': false,
      'category': productCategory,
      'expiryDate': productHasExpiry ? productExpiryDate : null
    };
    navigate('../Products', { state: newItem });
  };

  const items = (
    <>
      <div className="input-container">
        <InputFIeld
          containerStyle="input"
          inputStyle="input__text"
          label="Product Name"
          labelStyle="input__label"
          name="product-name"
          onChangeInput={onChangeName}
          placeholder="Enter Product Name"
          type="text"
          value={productName}
        />
        <InputFIeld
          containerStyle="input"
          inputStyle="input__text"
          label="Product Price"
          labelStyle="input__label"
          name="product-price"
          onChangeInput={onChangePrice}
          placeholder="Enter Price"
          type="text"
          value={productPrice}
        />
        <Select
          containerStyle="select"
          defaultValue="Chose Category"
          label="Select Item"
          labelStyle="select__label"
          name="select-item"
          onChangeSelect={onChangeCategory}
          selectStyle="select__items"
          value={productCategory}
        />
      </div>
      <div className="split">
        <TextArea
          containerStyle="textarea"
          label="Description"
          labelStyle="textarea__label"
          name="description"
          onChangeTextArea={onChangeDescription}
          placeholder="Enter Product Description"
          textAreatStyle="textarea__text"
          value={productDescription}
        />
        <div className="input__fileUpload">
          <div className="image-preview">
            {productImage === ''? 'No Image' : <img src={productImage} />}
          </div>
          <InputFIeld
            containerStyle="input"
            inputStyle="input__img"
            label="Upload Image"
            labelStyle="input__imglabel"
            name="product-img"
            onChangeInput={onChangeImage}
            placeholder="Upload file"
            type="file"
          />
        </div>
      </div>
      <InputFIeld
        containerStyle="checkbox"
        inputStyle="checkbox__text"
        label="Product has Expiry"
        labelStyle="checkbox__label"
        name="product-expiry"
        onChangeInput={onChangeHasExpiry}
        type="checkbox"
        value={productHasExpiry}
      />
      <InputFIeld
        containerStyle="input"
        inputStyle="input__date"
        isDisable={productHasExpiry}
        label="Date Expiry"
        labelStyle="input__label"
        name="date-expiry"
        onChangeInput={onChangeExpiryDate}
        type="date"
        value={productExpiryDate}
        
      />
    </>
  );

  return (
    <main className="new-product">
      <div className="path">
        {currentLocation.split('/').map((maping) => maping + ' / ')}
      </div>
      <hr className="devider" />
      <Form items={items} onSubmitHandler = {onSubmitHandler}/>
    </main>
  );
}
