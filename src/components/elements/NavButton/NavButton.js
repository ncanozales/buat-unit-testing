import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

export default function NavButton({ children, dest, icon, style }) {

  return (
    <>
      <Link to={dest}>
        <div className={style}>
          {icon&&<FontAwesomeIcon className="mr-2" icon={icon}/>}
          {children}
        </div>
      </Link>
    </>
  );
}

NavButton.propTypes = {
  children: PropTypes.node.isRequired,
  dest: PropTypes.string.isRequired,
  icon: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  style: PropTypes.string.isRequired,
};
