import { faLayerGroup, faSmile, faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import NavButton from '../NavButton/NavButton';

export default function SideNav() {
  const location = useLocation();
  const navLink = [
    { placeholder: 'Products', icon: faLayerGroup, dest: '/Products' },
    { placeholder: 'Users', icon: faUser, dest: '/Users' },
  ];

  return (
    <nav className="bg-[#1E293B] w-3/12 h-screen p-4 sticky top-0">
      <Link to="/"><FontAwesomeIcon className="mb-6" fontSize={40} icon={faSmile}/></Link>
      <h4 className="mb-2">Pages</h4>
      {navLink.map((menu, idx)=>{
        return (
          <NavButton dest={menu.dest} icon={menu.icon} key={idx} style={`flex p-3 w-full items-center mb-2 ${location.pathname.includes(menu.dest)? 'bg-[#0F172A]':''}`}>{menu.placeholder}</NavButton>);
      })}
    </nav>
  );
}
