import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown, faPaperPlane, faCircleExclamation, faComment, faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';

export default function Header() {

  return (
    <>
      <header className="px-8 py-4 bg-white text-[#475569] text-right w-full flex items-center justify-end font-bold  sticky top-0">
        <FontAwesomeIcon className="p-3 bg-[#F1F5F9] text-[#6B7280] mr-3 rounded-full" icon={faMagnifyingGlass}/>
        <FontAwesomeIcon className="p-3 bg-[#F1F5F9] text-[#6B7280] mr-3 rounded-full" icon={faComment}/>
        <FontAwesomeIcon className="p-3 bg-[#F1F5F9] text-[#6B7280] mr-3 rounded-full" icon={faCircleExclamation}/>
        <span className="w-[2px] mr-3 h-7 bg-[#E2E8F0]"/>
        <FontAwesomeIcon className="bg-[#334155] text-[#4338CA] p-3 rounded-full mr-3" icon={faPaperPlane}/>
        Acne
        <FontAwesomeIcon className="p-3 text-[#6B7280]" icon={faCaretDown}/>
      </header>
    </>
  );
}
